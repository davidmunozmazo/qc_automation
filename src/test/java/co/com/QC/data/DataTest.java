package co.com.QC.data;

import java.io.FileInputStream;
import java.util.Properties;

public class DataTest {
	
public static String strUrl;
public static String strReportName;
public static String strFrequency;
public static boolean inputActive = true;
public static String strCorreo;
public static String strCarrier;




public static String getStrCarrier() {
	return strCarrier;
}
public static void setStrCarrier(String strCarrier) {
	DataTest.strCarrier = strCarrier;
}
public static String getStrReportName() {
	return strReportName;
}
public static void setStrReportName(String strReportName) {
	DataTest.strReportName = strReportName;
}
public static String getStrFrequency() {
	return strFrequency;
}
public static void setStrFrequency(String strFrequency) {
	DataTest.strFrequency = strFrequency;
}
public static boolean isInputActive() {
	return inputActive;
}
public static void setInputActive(boolean inputActive) {
	DataTest.inputActive = inputActive;
}
public static String getStrCorreo() {
	return strCorreo;
}
public static void setStrCorreo(String strCorreo) {
	DataTest.strCorreo = strCorreo;
}
	public static String getStrUrl() {
		return strUrl;
	}
	public static void setStrUrl(String strUrl) {
		DataTest.strUrl = strUrl;
	}
	
	
	public static void obtenerDatos() {
		try {
			Properties props = new Properties();
			props.load(new FileInputStream("src/test/resources/properties/comunes.properties"));
			setStrUrl(props.getProperty("url"));
			setStrCorreo(props.getProperty("correoReporte"));
			setStrReportName(props.getProperty("reporteName"));
			setStrFrequency(props.getProperty("frequencyReport"));
			setStrCarrier(props.getProperty("carrier"));
		} catch (Exception e) {
			
		}
	
	}	
	

}
