package co.com.QC.definitions;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import static cucumber.api.SnippetType.CAMELCASE;
@CucumberOptions(
plugin = {"pretty", "html:target/cucumber-html-report"},
snippets = CAMELCASE, glue = "co/com/QC/definitions", tags = "@Funcional",
features="src/test/resources/features/login.feature")

@RunWith(CucumberWithSerenity.class )

public class LoginRunner {
	
	

}
