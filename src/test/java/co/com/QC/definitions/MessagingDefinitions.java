package co.com.QC.definitions;

import co.com.QC.steps.MessagingSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class MessagingDefinitions {
	
	@Steps
	MessagingSteps messagingSteps;
	
	@Given("^doy click en el boton de messaging$")
	public void doyClickEnElBotonDeMessaging() throws Exception {
		messagingSteps.ingresarModulo();
	   
	}


	@When("^busco \"([^\"]*)\" carrier$")
	public void buscoCarrier(String Carrier) throws Exception {
	messagingSteps.buscarCarrier(Carrier);    
	}

	@When("^seleccion el carrier$")
	public void seleccionElCarrier() throws Exception {
	messagingSteps.seleccionarCarrier();
	}

	@When("^seleccion uno de los contactos$")
	public void seleccionUnoDeLosContactos() throws Exception {
		messagingSteps.seleccionarContactos();
	   
	}

	@Then("^redacto el mensaje y envio el mensaje$")
	public void redactoElMensajeYEnvioElMensaje() throws Exception {
		messagingSteps.enviarMensaje();
	  
	}

}
