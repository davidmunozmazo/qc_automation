package co.com.QC.definitions;

import co.com.QC.steps.MyCarrierSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class MyCarrierDefinitions {
	
	@Steps
	MyCarrierSteps myCarrierSteps;
	
	@Given("^doy click en el boton de my carrier$")
	public void doyClickEnElBotonDeMyCarrier() throws Exception {
		myCarrierSteps.darClickEnBoton();
	}


	@When("^doy click en adicionar carrier$")
	public void doyClickEnAdicionarCarrier() throws Exception {
	    myCarrierSteps.addCarrier();
	}

	@When("^busco el carrier a seleccionar \"([^\"]*)\"  y espero$")
	public void buscoElCarrierASeleccionarYEspero(String carrier) throws Exception {
		myCarrierSteps.buscarCarrier(carrier);
	    
	}

	@When("^Adiciono a la lista de my carriers$")
	public void adicionoALaListaDeMyCarriers() throws Exception {
		myCarrierSteps.adicionarALaLista();
		
	    
	}

	@When("^realizo la busueda$")
	public void realizoLaBusueda() throws Exception {
		myCarrierSteps.realizarBusqueda();
	    
	}

	@Then("^valido que el \"([^\"]*)\" se encuentra en la lista$")
	public void validoQueElSeEncuentraEnLaLista(String carrier) throws Exception {
	    myCarrierSteps.encuentraEnLista(carrier);
	}

}
