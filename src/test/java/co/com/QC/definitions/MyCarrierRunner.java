package co.com.QC.definitions;

import static cucumber.api.SnippetType.CAMELCASE;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@CucumberOptions(
plugin = {"pretty", "html:target/cucumber-html-report"},
snippets = CAMELCASE, glue = "co/com/QC/definitions", tags = "@funcional",
features="src/test/resources/features/myCarrier.feature")
@RunWith(CucumberWithSerenity.class )


public class MyCarrierRunner {
	
	

}
