package co.com.QC.definitions;

import co.com.QC.steps.MyGroupsSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class MyGroupsDefinitios {
	
	
	@Steps
	MyGroupsSteps myGroupsSteps;
	
	
	@Given("^doy click en el boton de my groups$")
	public void doyClickEnElBotonDeMyGroups() throws Exception {
		myGroupsSteps.ingresarAlmoduloGroups();
	}


	@When("^doy click en adicionar groups$")
	public void doyClickEnAdicionarGroups() throws Exception {
		myGroupsSteps.ingresarGroups();
	}

	@When("^ingreso la informacion para el nuevo groups$")
	public void ingresoLaInformacionParaElNuevoGroups() throws Exception {
	 myGroupsSteps.ingresarInformacion();   
	}

	@When("^busco el carrier para asociar el groups$")
	public void buscoElCarrierParaAsociarElGroups() throws Exception {
	    myGroupsSteps.buscarCarrier();
	}

	@When("^valido que el groups es creado$")
	public void validoQueElGroupsEsCreado() throws Exception {
		myGroupsSteps.validarcreacion();
	    
	}

	@Then("^elimino el groups$")
	public void eliminoElGroups() throws Exception {
		myGroupsSteps.EliminarGrupo();
	    
	}
	
	

}
