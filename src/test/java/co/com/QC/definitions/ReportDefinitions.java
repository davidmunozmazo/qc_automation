package co.com.QC.definitions;

import co.com.QC.steps.ReportSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class ReportDefinitions {
	
	@Steps
	ReportSteps reportStep;
	
	@Given("^doy click en el boton del reporte$")
	public void doy_click_en_el_boton_del_reporte() {
		reportStep.clickEnBotton();	
	    
	}

	@When("^doy click en adicionar nuevo reporte$")
	public void doy_click_en_adicionar_nuevo_reporte() {
		reportStep.adicionarReporte();
	    
	}

	@Then("^ingreso la informacion para el nuevo reporte$")
	public void ingreso_la_informacion_para_el_nuevo_reporte() {
		reportStep.ingresarInformacion();
	    
	}

	@Then("^busco el nuevo reporte y ejecuto el reporte$")
	public void busco_el_nuevo_reporte_y_ejecuto_el_reporte() {
		reportStep.ejecutarReporte();
	    
	    
	}

	@Then("^valido que es creado$")
	public void valido_que_es_creado() {
	    reportStep.validarCreacion();
	}


}
