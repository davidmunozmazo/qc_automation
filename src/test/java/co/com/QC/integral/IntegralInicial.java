package co.com.QC.integral;

import org.junit.AfterClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import co.com.QC.util.MetodosComunes;

@RunWith(Suite.class)
@SuiteClasses({
	QCInt.class
	})

public class IntegralInicial {
	
	@AfterClass
	public static void generarResultadoFinal(){
		MetodosComunes.asignarResultadoEjecucion();
	}	

}
