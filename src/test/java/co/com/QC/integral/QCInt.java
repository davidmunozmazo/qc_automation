package co.com.QC.integral;

import org.junit.Test;
import org.junit.runner.JUnitCore;

import co.com.QC.definitions.LoginRunner;
import co.com.QC.definitions.MessagingRunner;
import co.com.QC.definitions.MyCarrierRunner;
import co.com.QC.definitions.MyGroupsRunner;
import co.com.QC.definitions.ReportRunner;
import co.com.QC.util.MetodosComunes;

public class QCInt {
	
	@Test
	public void realizarEjecucion() {
		
		try {
			MetodosComunes.verificacionResultadoBuild(JUnitCore.runClasses(LoginRunner.class)); 
			MetodosComunes.verificacionResultadoBuild(JUnitCore.runClasses(ReportRunner.class));
			MetodosComunes.verificacionResultadoBuild(JUnitCore.runClasses(MyGroupsRunner.class));
			MetodosComunes.verificacionResultadoBuild(JUnitCore.runClasses(MyCarrierRunner.class));
			MetodosComunes.verificacionResultadoBuild(JUnitCore.runClasses(MessagingRunner.class));
		}
		catch (Exception e) {
			
		}
		
	}
	
	

}
