package co.com.QC.page;

import java.util.logging.Level;

import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.support.FindBy;

import co.com.QC.util.DefinicionUtilidades;
import co.com.QC.util.Mdl_variables;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class LoginPage extends PageObject {
	
	SoftAssertions softAssertions = new SoftAssertions();
	
		
	@FindBy(id = "mat-input-0")
	private WebElementFacade txtEmail;
	
	@FindBy(id = "mat-input-1")
	private WebElementFacade txtPassword;
	
	
	@FindBy(xpath = "//button[@class='continue-button mat-flat-button mat-primary ng-star-inserted']")
	private WebElementFacade btnLogin;
	
	@FindBy(xpath = "//*[@id=\'toolbar\']/div/span[1]/h1")
	private static WebElementFacade lbldashboard;
	

	public void ingresarUrl(String url) {

		try {
			getDriver().manage().window().maximize();
			getDriver().get(url);
			waitFor(5).seconds();
		} catch (Exception e) {
			Mdl_variables.LOGGERTRANSVERSAL.log(Level.SEVERE, e.getMessage());
		}

	}
	public void ingresarCredenciales(String email, String password) {
		try {
			
			txtEmail.click();
			txtEmail.sendKeys(email);
			waitFor(1).seconds();
			getDriver().findElement(By.xpath("//button[@class='continue-button mat-flat-button mat-primary ng-star-inserted']")).click();
			txtPassword.click();
			txtPassword.sendKeys(password);
			getDriver().findElement(By.xpath("//button[@class='continue-button mat-flat-button mat-primary ng-star-inserted']")).click();
			
			waitFor(1).seconds();
			btnLogin.click();
			
		} catch (Exception e) {
			Mdl_variables.LOGGERTRANSVERSAL.log(Level.SEVERE, e.getMessage());
		}
		
	}
	
	public void verificarLabel(String label) {

		try {
			String validar = lbldashboard.getText();
			
			if (validar.equalsIgnoreCase(label)) {

				DefinicionUtilidades.softAssertionsDefinicion.assertThat(validar).as(String
						.format("el valor esperado %s correponde al de pantalla que es %s", validar, label))
						.isEqualTo(label);
				Mdl_variables.boolAutenticado = true;
			
			}
			
		} catch (Exception e) {
			Mdl_variables.LOGGERTRANSVERSAL.log(Level.SEVERE, e.getMessage());
			softAssertions.assertAll();
		}
					
		
		
	}
	
	

}
