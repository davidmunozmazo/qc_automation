package co.com.QC.page;

import java.util.logging.Level;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.FindBy;

import co.com.QC.util.DefinicionUtilidades;
import co.com.QC.util.Mdl_variables;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class MessagingPage extends PageObject {

	@FindBy(xpath =  "/html[1]/body[1]/app-root[1]/app-content[1]/mat-sidenav-container[1]/mat-sidenav[1]/div[1]/div[1]/app-sidenav[1]/div[1]/div[3]/div[7]/a[1]")
	private WebElementFacade btnMessaging;
	
	@FindBy(xpath =  "/html/body/app-root/app-content/mat-sidenav-container/mat-sidenav-content/div/mat-card/app-messaging/div/div[1]/div/div[2]/a")
	private WebElementFacade linkAddMessaging;
	
	@FindBy(xpath =  "/html[1]/body[1]/app-root[1]/app-content[1]/mat-sidenav-container[1]/mat-sidenav-content[1]/div[1]/mat-card[1]/app-new-message-container[1]/mat-horizontal-stepper[1]/div[2]/div[1]/app-select-carrier[1]/mat-card[1]/div[1]/div[1]/div[2]/input[1]")
	private WebElementFacade txtBuscarCarrier;
	
	@FindBy(xpath =  "/html[1]/body[1]/app-root[1]/app-content[1]/mat-sidenav-container[1]/mat-sidenav-content[1]/div[1]/mat-card[1]/app-new-message-container[1]/mat-horizontal-stepper[1]/div[2]/div[1]/app-select-carrier[1]/mat-card[1]/div[1]/mat-card[1]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/mat-checkbox[1]/label[1]/div[1]")
	private WebElementFacade checkBoxCarrier;
	
	@FindBy(xpath =  "//button[@class='animated FadeIn mat-raised-button mat-primary']")
	private WebElementFacade btnSeleccionarCarrier;
	
	@FindBy(xpath =  "/html[1]/body[1]/app-root[1]/app-content[1]/mat-sidenav-container[1]/mat-sidenav-content[1]/div[1]/mat-card[1]/app-new-message-container[1]/mat-horizontal-stepper[1]/div[2]/div[2]/app-select-contact[1]/mat-card[1]/div[2]/app-buttons-message[2]/button[1]")
	private WebElementFacade btnSeleccionarContact;
	
	@FindBy(xpath =  "/html[1]/body[1]/app-root[1]/app-content[1]/mat-sidenav-container[1]/mat-sidenav-content[1]/div[1]/mat-card[1]/app-new-message-container[1]/mat-horizontal-stepper[1]/div[2]/div[3]/app-send-email[1]/mat-card[1]/form[1]/mat-form-field[1]/div[1]/div[1]/div[1]/input[1]")
	private WebElementFacade txtMessage;
	
	@FindBy(xpath =  "//mat-chip[contains(text(),'Carrier Name')]")
	private WebElementFacade btnCarrierName;
	
	@FindBy(xpath =  "/html[1]/body[1]/app-root[1]/app-content[1]/mat-sidenav-container[1]/mat-sidenav-content[1]/div[1]/mat-card[1]/app-new-message-container[1]/mat-horizontal-stepper[1]/div[2]/div[3]/app-send-email[1]/mat-card[1]/div[1]/div[1]/ckeditor[1]/div[2]/div[2]/div[1]")
	private WebElementFacade txtAreaMessage;
	
	@FindBy(xpath =  "/html[1]/body[1]/app-root[1]/app-content[1]/mat-sidenav-container[1]/mat-sidenav-content[1]/div[1]/mat-card[1]/app-new-message-container[1]/mat-horizontal-stepper[1]/div[2]/div[3]/app-send-email[1]/mat-card[1]/div[1]/div[1]/div[1]/button[1]")
	private WebElementFacade btnEnviarMensaje;
	
	@FindBy(xpath =  "//p[contains(text(),'The message has been successfully submitted')]")
	private WebElementFacade labelMensajeConfirmacion;
	
	@FindBy(xpath =  "/html[1]/body[1]/div[2]/div[2]/div[1]/mat-dialog-container[1]/app-dialog-c[1]/mat-dialog-actions[1]/div[1]/button[1]")
	private WebElementFacade btnCerrar;
	
	
	
	public void ingresarModulo() {
	try {
		btnMessaging.click();
		waitFor(2).seconds();
		linkAddMessaging.click();
	} catch (Exception e) {
		Mdl_variables.LOGGERTRANSVERSAL.log(Level.SEVERE, e.getMessage());
		DefinicionUtilidades.softAssertionsDefinicion.assertAll();
	}
		
	}

	public void buscarCarrier(String carrier) {
	try {
		txtBuscarCarrier.click();
		txtBuscarCarrier.sendKeys(carrier);
		txtBuscarCarrier.sendKeys(Keys.ENTER);
		
	} catch (Exception e) {
		Mdl_variables.LOGGERTRANSVERSAL.log(Level.SEVERE, e.getMessage());
		DefinicionUtilidades.softAssertionsDefinicion.assertAll();
		
	}
		
		
	}

	public void seleccionar() {
		try {
			checkBoxCarrier.click();
			btnSeleccionarCarrier.click();
			
		} catch (Exception e) {
			Mdl_variables.LOGGERTRANSVERSAL.log(Level.SEVERE, e.getMessage());
			DefinicionUtilidades.softAssertionsDefinicion.assertAll();
		}
		
	}

	public void seleccionoContacto(int contacto) {
		
		contacto = 1;
		try {
			
			getDriver().findElement(By.xpath("/html[1]/body[1]/app-root[1]/app-content[1]/mat-sidenav-container[1]/mat-sidenav-content[1]/div[1]/mat-card[1]/app-new-message-container[1]/mat-horizontal-stepper[1]/div[2]/div[2]/app-select-contact[1]/mat-card[1]/div[1]/mat-checkbox["+contacto+"]/label[1]/div[1]")).click();
			waitFor(2).seconds();
			btnSeleccionarContact.click();
		} catch (Exception e) {
			Mdl_variables.LOGGERTRANSVERSAL.log(Level.SEVERE, e.getMessage());
			DefinicionUtilidades.softAssertionsDefinicion.assertAll();
		}
	}

	public void enviarMensaje() {
		try {
			txtMessage.click();
			txtMessage.sendKeys("This is a Test");
			btnCarrierName.click();
			txtAreaMessage.click();
			txtAreaMessage.sendKeys("This is a test");
			btnEnviarMensaje.click();
			
			boolean valido = labelMensajeConfirmacion.isVisible();
			if(!valido) {
			DefinicionUtilidades.softAssertionsDefinicion.fail("El mensaje no fue enviado al contacto del carrier");
			}
			
			btnCerrar.click();
			
			
			
		} catch (Exception e) {
			Mdl_variables.LOGGERTRANSVERSAL.log(Level.SEVERE, e.getMessage());
			DefinicionUtilidades.softAssertionsDefinicion.assertAll();
		}
		
	}
	
	
}
