package co.com.QC.page;

import java.util.List;
import java.util.logging.Level;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import co.com.QC.util.DefinicionUtilidades;
import co.com.QC.util.Mdl_variables;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class MyCarrierPage extends PageObject {
	
	private static String strCarrier = "";
	
	@FindBy(xpath =  "/html[1]/body[1]/app-root[1]/app-content[1]/mat-sidenav-container[1]/mat-sidenav[1]/div[1]/div[1]/app-sidenav[1]/div[1]/div[3]/div[3]/a[1]")
	private WebElementFacade btnMyCarrier;
	
	
	@FindBy(xpath =  "//a[@class='add-carrier-link']")
	private WebElementFacade lnkAddCarrier;
	
	
	@FindBy(xpath =  "/html[1]/body[1]/app-root[1]/app-content[1]/mat-sidenav-container[1]/mat-sidenav-content[1]/div[1]/mat-card[1]/app-carrier-add[1]/div[1]/div[1]/div[1]/div[1]/div[1]/mat-form-field[1]/div[1]/div[1]/div[1]/input[1]")
	private WebElementFacade txtBuscarCarrier;
	
	@FindBy(xpath =  "/html[1]/body[1]/app-root[1]/app-content[1]/app-toolbar[1]/mat-toolbar[1]/div[1]/span[1]/h1[1]")
	private WebElementFacade lblVolverAtras;	
	
	
	@FindBy(xpath =  "//input[@placeholder='Carrier Name, DOT#, MC#']")
	private WebElementFacade txtBusquedaCarrierPorNombre;	
	
	@FindBy(xpath =  "//button[@class='qc-template mat-button']")
	private WebElementFacade btnBuscarCarrier;
	
	@FindBy(xpath =  "/html/body/app-root/app-content/mat-sidenav-container/mat-sidenav-content/div/mat-card/app-carriers/div/div[1]/div/div[2]/div/div/div[1]/div")
	private WebElementFacade linkLimpiarFiltros;
	
	
	

	public void darclickBoton() {
		try {
			btnMyCarrier.click();
			
			
		} catch (Exception e) {
			Mdl_variables.LOGGERTRANSVERSAL.log(Level.SEVERE, e.getMessage());
			DefinicionUtilidades.softAssertionsDefinicion.assertAll();
		}
		
	}

	public void addCarrier() {
		try {
			lnkAddCarrier.click();
			
		} catch (Exception e) {
			Mdl_variables.LOGGERTRANSVERSAL.log(Level.SEVERE, e.getMessage());
			DefinicionUtilidades.softAssertionsDefinicion.assertAll();
			
		}
	
	}

	public void buscarCarrier(String carrier) {
		
		strCarrier = carrier;
		try {
			waitFor(10).seconds();
			txtBuscarCarrier.click();
			txtBuscarCarrier.clear();
			txtBuscarCarrier.sendKeys(carrier);
			txtBuscarCarrier.sendKeys(Keys.ENTER);
			waitFor(50).seconds();
			
		} catch (Exception e) {
			Mdl_variables.LOGGERTRANSVERSAL.log(Level.SEVERE, e.getMessage());
			DefinicionUtilidades.softAssertionsDefinicion.assertAll();
		}
		
	}

	public void adicionarLista() {
		boolean encontrado = false;
		int i = 1;
		
	try {

		WebElement resultTable = findBy("//*[@id=\'add-carrier-container\']/mat-card/table/tbody");
		
		List<WebElement> rows = resultTable.findElements(By.xpath("//*[@id=\'add-carrier-container\']/mat-card/table/tbody/tr[1]"));
for (WebElement row : rows) {
			
			List<WebElement> cells = row.findElements(By.tagName("td"));
			if (cells.get(0).getText().equals(strCarrier)) {
				waitFor(2).seconds();
				cells.get(6).click();
				getDriver().findElement(By.xpath("//button[contains(text(),'Add to carrier list')]")).click();
				encontrado = true; 
				break;
			}
}
	} catch (Exception e) {
		Mdl_variables.LOGGERTRANSVERSAL.log(Level.SEVERE, e.getMessage());
		DefinicionUtilidades.softAssertionsDefinicion.assertAll();
	}
		
	}

	public void realizarBusqueda() {
		
		try {
			String StrCarrierUperCase = strCarrier.toUpperCase();
			waitFor(5).seconds();
			btnMyCarrier.click();
			//lblVolverAtras.click();
			txtBusquedaCarrierPorNombre.click();
			txtBusquedaCarrierPorNombre.sendKeys(StrCarrierUperCase);
			//btnBuscarCarrier.sendKeys(Keys.ENTER);
			btnBuscarCarrier.click();
			linkLimpiarFiltros.click();
			
			
		} catch (Exception e) {
			Mdl_variables.LOGGERTRANSVERSAL.log(Level.SEVERE, e.getMessage());
			DefinicionUtilidades.softAssertionsDefinicion.assertAll();
		}

	}

	public void entrarEnLaLista(String carrier) {
		
		String StrCarrierUperCase = carrier.toUpperCase();
		boolean encontrado = false; 
	
		try {
			
		 for (int i = 1; i < 10; i++) {
			 			 
			String txtValue = getDriver().findElement(By.xpath("/html/body/app-root/app-content/mat-sidenav-container/mat-sidenav-content/div/mat-card/app-carriers/div/div[2]/table/tbody/tr["+i+"]/td/div/div/div[1]/span")).getText();	 
			if (txtValue.equals(StrCarrierUperCase)) {
				
				DefinicionUtilidades.softAssertionsDefinicion.assertThat(txtValue)
				.as(String.format("El nombre del Carrier se encuentra  %s correponde al de pantalla que es %s",
						txtValue, StrCarrierUperCase))
				.isEqualTo(StrCarrierUperCase);
				encontrado = true;
				break;
		} 
				
		}
		 
		}catch (Exception e) {
			Mdl_variables.LOGGERTRANSVERSAL.log(Level.SEVERE, e.getMessage());
			DefinicionUtilidades.softAssertionsDefinicion.assertAll();
		}
	}
	
	
	
	
	
	

}
