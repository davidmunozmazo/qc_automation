package co.com.QC.page;

import java.util.logging.Level;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.util.List;

import co.com.QC.data.DataTest;
import co.com.QC.util.DefinicionUtilidades;
import co.com.QC.util.Mdl_variables;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class MyGroupsPage extends PageObject {
	
	@FindBy(xpath = "//div[4]//a[1]")
	private WebElementFacade btnMyGroups;
	
	@FindBy(xpath = "//a[@class='add-carrier-link']")
	private WebElementFacade linkAddGroups;
	
	@FindBy(xpath =  "/html[1]/body[1]/app-root[1]/app-content[1]/mat-sidenav-container[1]/mat-sidenav-content[1]/div[1]/mat-card[1]/app-add-group[1]/div[1]/mat-vertical-stepper[1]/div[1]/div[1]/div[1]/div[1]/form[1]/div[1]/div[1]/mat-form-field[1]/div[1]/div[1]/div[1]/input[1]")
	private WebElementFacade txtGroupsName;
	
	@FindBy(xpath =   "/html[1]/body[1]/app-root[1]/app-content[1]/mat-sidenav-container[1]/mat-sidenav-content[1]/div[1]/mat-card[1]/app-add-group[1]/div[1]/mat-vertical-stepper[1]/div[1]/div[1]/div[1]/div[1]/form[1]/div[2]/div[1]/mat-form-field[1]/div[1]/div[1]/div[1]/textarea[1]")
	private WebElementFacade txtGroupsDescriptions;
	
	@FindBy(xpath = "/html[1]/body[1]/app-root[1]/app-content[1]/mat-sidenav-container[1]/mat-sidenav-content[1]/div[1]/mat-card[1]/app-add-group[1]/div[1]/mat-vertical-stepper[1]/div[1]/div[1]/div[1]/div[1]/div[2]/button[1]")
	private WebElementFacade btnNext;
	
	@FindBy(xpath = "//input[@placeholder='Search groups by name']")
	private WebElementFacade txtBuscarCarrier;
	
	@FindBy(xpath = "//button[@class='qc-template mat-button']")
	private WebElementFacade btnBuscarCarrier;
		
	public void ingresarModuloGroups() {
		try {
		
			btnMyGroups.click();
			
		} catch (Exception e) {
			Mdl_variables.LOGGERTRANSVERSAL.log(Level.SEVERE, e.getMessage());
			DefinicionUtilidades.softAssertionsDefinicion.assertAll();
			
		}
		
	}

	public void adicionarGroups() {
		try {
			linkAddGroups.click();
					
		} catch (Exception e) {
			Mdl_variables.LOGGERTRANSVERSAL.log(Level.SEVERE, e.getMessage());
			DefinicionUtilidades.softAssertionsDefinicion.assertAll();
		
		}
		
	}

	public void ingresarInformacion(String strReportName) {
		try {
			txtGroupsName.click();
			
			txtGroupsName.sendKeys(strReportName);
			txtGroupsDescriptions.click();
			txtGroupsDescriptions.sendKeys(strReportName);		
			btnNext.click();
			
		} catch (Exception e) {
			Mdl_variables.LOGGERTRANSVERSAL.log(Level.SEVERE, e.getMessage());
			DefinicionUtilidades.softAssertionsDefinicion.assertAll();
		}
		
	}

	public void buscarCarrier(String strCarrier) {
		boolean encontrado = false;
		int i = 1;
	try {
			
			getDriver().findElement(By.xpath("/html[1]/body[1]/app-root[1]/app-content[1]/mat-sidenav-container[1]/mat-sidenav-content[1]/div[1]/mat-card[1]/app-add-group[1]/div[1]/mat-vertical-stepper[1]/div[2]/div[1]/div[1]/div[1]/form[1]/div[1]/app-list-carrier[1]/mat-card[1]/div[1]/div[1]/div[2]/input[1]")).click();
			getDriver().findElement(By.xpath("/html[1]/body[1]/app-root[1]/app-content[1]/mat-sidenav-container[1]/mat-sidenav-content[1]/div[1]/mat-card[1]/app-add-group[1]/div[1]/mat-vertical-stepper[1]/div[2]/div[1]/div[1]/div[1]/form[1]/div[1]/app-list-carrier[1]/mat-card[1]/div[1]/div[1]/div[2]/input[1]")).sendKeys(strCarrier);
			waitFor(2).seconds();
			getDriver().findElement(By.xpath("/html[1]/body[1]/app-root[1]/app-content[1]/mat-sidenav-container[1]/mat-sidenav-content[1]/div[1]/mat-card[1]/app-add-group[1]/div[1]/mat-vertical-stepper[1]/div[2]/div[1]/div[1]/div[1]/form[1]/div[1]/app-list-carrier[1]/mat-card[1]/div[1]/div[1]/div[2]/input[1]")).sendKeys(Keys.ENTER);
			
			
			
			WebElement resultTable = findBy("/html[1]/body[1]/app-root[1]/app-content[1]/mat-sidenav-container[1]/mat-sidenav-content[1]/div[1]/mat-card[1]/app-add-group[1]/div[1]/mat-vertical-stepper[1]/div[2]/div[1]/div[1]/div[1]/form[1]/div[1]/app-list-carrier[1]/mat-card[1]/div[1]/mat-card[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/table[1]/tbody[1]");
			
			List<WebElement> rows = resultTable.findElements(By.xpath("//body//tbody//tr[1]"));
for (WebElement row : rows) {
				
				List<WebElement> cells = row.findElements(By.tagName("td"));
				if (cells.get(3).getText().equals("123 Mount Pleasant Drive")) {
					waitFor(2).seconds();
					getDriver().findElement(By.xpath("/html[1]/body[1]/app-root[1]/app-content[1]/mat-sidenav-container[1]/mat-sidenav-content[1]/div[1]/mat-card[1]/app-add-group[1]/div[1]/mat-vertical-stepper[1]/div[2]/div[1]/div[1]/div[1]/form[1]/div[1]/app-list-carrier[1]/mat-card[1]/div[1]/mat-card[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/mat-checkbox[1]/label[1]/div[1]")).click();
					
					encontrado = true;
					break;
				}
				i = i +1;
}
				if (!encontrado) {
					DefinicionUtilidades.softAssertionsDefinicion.fail("El carrier" + strCarrier + "que se encuentra en BD no esta en la pantalla");
				}
				getDriver().findElement(By.xpath("/html[1]/body[1]/app-root[1]/app-content[1]/mat-sidenav-container[1]/mat-sidenav-content[1]/div[1]/mat-card[1]/app-add-group[1]/div[1]/mat-vertical-stepper[1]/div[2]/div[1]/div[1]/div[1]/form[1]/div[1]/app-list-carrier[1]/mat-card[1]/div[1]/div[2]/button[2]")).click();

				
		} catch (Exception e) {
			Mdl_variables.LOGGERTRANSVERSAL.log(Level.SEVERE, e.getMessage());
			DefinicionUtilidades.softAssertionsDefinicion.assertAll();
			
		}
		
	}

	public void validarCreacion(String strReportName) {
		
		boolean encontrado = false;
		
		try {
			waitFor(2).seconds();
			txtBuscarCarrier.click();
			txtBuscarCarrier.sendKeys(strReportName);
			btnBuscarCarrier.click();
			waitFor(3).seconds();
			
		WebElement resultTable = findBy("/html/body/app-root/app-content/mat-sidenav-container/mat-sidenav-content/div/mat-card/app-carriers-group/div/mat-card/table/tbody");
			
			List<WebElement> rows = resultTable.findElements(By.xpath("/html/body/app-root/app-content/mat-sidenav-container/mat-sidenav-content/div/mat-card/app-carriers-group/div/mat-card/table/tbody/tr"));
for (WebElement row : rows) {
				
				List<WebElement> cells = row.findElements(By.tagName("td"));
				if (cells.get(0).getText().equals(strReportName)) {
					
					DefinicionUtilidades.softAssertionsDefinicion.assertThat(cells.get(0).getText())
					.as(String.format("El nombre del grupo esperado %s correponde al de pantalla que es %s",
							cells.get(0).getText(), strReportName))
					.isEqualTo(strReportName);
					encontrado = true;
					break;
				}
				
}
				if (!encontrado) {
					DefinicionUtilidades.softAssertionsDefinicion.fail("El Group" + strReportName + "que se encuentra en BD no esta en la pantalla");
				}	
				
				
	} catch (Exception e) {
		Mdl_variables.LOGGERTRANSVERSAL.log(Level.SEVERE, e.getMessage());
		DefinicionUtilidades.softAssertionsDefinicion.assertAll();
			
		}
		
	}

	public void eliminarGrupo() {
	try {
		getDriver().findElement(By.xpath("/html/body/app-root/app-content/mat-sidenav-container/mat-sidenav-content/div/mat-card/app-carriers-group/div/mat-card/table/tbody/tr/td[3]/div/div[3]")).click();
		waitFor(2).seconds();
		getDriver().findElement(By.xpath("//*[@id=\'btn-dialog-confirm\']")).click();
		
	}
	
	catch (Exception e) {
		Mdl_variables.LOGGERTRANSVERSAL.log(Level.SEVERE, e.getMessage());
		DefinicionUtilidades.softAssertionsDefinicion.assertAll();
		
	}
		
	}
	
}
