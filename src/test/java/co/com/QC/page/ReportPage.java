package co.com.QC.page;

import java.util.logging.Level;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import co.com.QC.util.DefinicionUtilidades;
import co.com.QC.util.Mdl_variables;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class ReportPage extends PageObject {

	@FindBy(xpath = "//a[@class='add-carrier-link']")
	private WebElementFacade lnkAddReport;

	@FindBy(id = "mat-input-3")
	private WebElementFacade txtReportName;

	@FindBy(xpath = "//div[@class='properties']//div//button[@class='mat-flat-button mat-primary']")
	private WebElement btnSaveReport;

	@FindBy(xpath = "//span[@class='search-text']")
	private WebElement lblCreateReport;

	@FindBy(xpath = "//html/body/app-root/app-content/mat-sidenav-container/mat-sidenav-content/div/mat-card/app-reports/div/div[2]/mat-card/mat-card-content/div/div[2]/div[1]/span")
	private WebElement lbltableReportName;

	@FindBy(xpath = "//mat-icon[contains(text(),'play_circle_outline')]")
	private WebElement lbltableReportExecute;

	@FindBy(xpath = "//*[@id=\'cdk-overlay-5\']/snack-bar-container/simple-snack-bar/div/button/span")
	private WebElement lblOK;

	@FindBy(id = "btn-dialog-confirm")
	private WebElement btnConfirmaDelete;

	@FindBy(id = "mat-checkbox-1-input")
	private WebElement btnActive;

	public void clickEnBotonReport() {
		try {

			getDriver().findElement(By.xpath(
					"/html/body/app-root/app-content/mat-sidenav-container/mat-sidenav/div/div/app-sidenav/div/div[3]/div[2]/a"))
					.click();
			waitFor(5).seconds();
		} catch (Exception e) {
			Mdl_variables.LOGGERTRANSVERSAL.log(Level.SEVERE, e.getMessage());
			DefinicionUtilidades.softAssertionsDefinicion.assertAll();
		}
	}

	public void agregarReporte() {
		try {
			waitFor(2).seconds();
			lnkAddReport.click();

		} catch (Exception e) {
			Mdl_variables.LOGGERTRANSVERSAL.log(Level.SEVERE, e.getMessage());
			DefinicionUtilidades.softAssertionsDefinicion.assertAll();

		}

	}

	public void adicionarReporte(String strCorreo, String strFrequency, String strReportName, boolean inputActive) {
		try {
			txtReportName.click();
			txtReportName.sendKeys(strReportName);
			getDriver().findElement(By.xpath("/html[1]/body[1]/app-root[1]/app-content[1]/mat-sidenav-container[1]/mat-sidenav-content[1]/div[1]/mat-card[1]/app-report-setting[1]/div[1]/div[2]/div[1]/app-report-properties[1]/mat-card[1]/form[1]/div[1]/div[2]/div[1]/mat-checkbox[1]/label[1]/div[1]")).click();
			// btnActive.click();
			getDriver().findElement(By.xpath(
					"/html[1]/body[1]/app-root[1]/app-content[1]/mat-sidenav-container[1]/mat-sidenav-content[1]/div[1]/mat-card[1]/app-report-setting[1]/div[1]/div[2]/div[1]/app-report-properties[1]/mat-card[1]/form[1]/div[1]/div[2]/div[1]/mat-form-field[1]/div[1]/div[1]/div[1]/mat-select[1]/div[1]/div[1]/span[1]"))
					.click();
			getDriver().findElement(By.xpath("/html[1]/body[1]/div[2]/div[2]/div[1]/div[1]/div[1]/mat-option[1]"))
					.click();
			getDriver().findElement(By.xpath(
					"/html[1]/body[1]/app-root[1]/app-content[1]/mat-sidenav-container[1]/mat-sidenav-content[1]/div[1]/mat-card[1]/app-report-setting[1]/div[1]/div[2]/div[1]/app-report-properties[1]/mat-card[1]/form[1]/div[1]/div[3]/mat-form-field[1]/div[1]/div[1]/div[1]/mat-chip-list[1]/div[1]/input[1]"))
					.click();
			getDriver().findElement(By.xpath(
					"/html[1]/body[1]/app-root[1]/app-content[1]/mat-sidenav-container[1]/mat-sidenav-content[1]/div[1]/mat-card[1]/app-report-setting[1]/div[1]/div[2]/div[1]/app-report-properties[1]/mat-card[1]/form[1]/div[1]/div[3]/mat-form-field[1]/div[1]/div[1]/div[1]/mat-chip-list[1]/div[1]/input[1]"))
					.sendKeys(strCorreo);
			getDriver().findElement(By.xpath("/html[1]/body[1]/app-root[1]/app-content[1]/mat-sidenav-container[1]/mat-sidenav-content[1]/div[1]/mat-card[1]/app-report-setting[1]/div[1]/div[1]/mat-card[1]/div[2]/button[1]")).click();
			
			
			
			
			
			
			

			btnSaveReport.click();
			waitFor(2).seconds();

			String strViewListReport = lblCreateReport.getText().substring(0, 9);

			DefinicionUtilidades.softAssertionsDefinicion.assertThat("Create or")
					.as(String.format("El nombre de la seccion esperada %s correponde al de pantalla que es %s",
							"Create or", strViewListReport))
					.isEqualTo(strViewListReport);

		} catch (Exception e) {
			Mdl_variables.LOGGERTRANSVERSAL.log(Level.SEVERE, e.getMessage());
			DefinicionUtilidades.softAssertionsDefinicion.assertAll();

		}

	}

	public void ejecutarReporte() {
		try {

			String validarName = lbltableReportName.getText();

			DefinicionUtilidades.softAssertionsDefinicion.assertThat("Testing")
					.as(String.format("El nombre de la reporte esperada %s no correponde al de pantalla que es %s",
							"Testing", validarName))
					.isEqualTo(validarName);

			waitFor(3).seconds();
		} catch (Exception e) {
			Mdl_variables.LOGGERTRANSVERSAL.log(Level.SEVERE, e.getMessage());
			DefinicionUtilidades.softAssertionsDefinicion.assertAll();

		}

	}

	public void validarCreacion() {
		try {

			// lbltableReportExecute.click();
			
			getDriver().findElement(By.xpath("//mat-icon[contains(text(),'play_circle_outline')]")).click();

//			String validarName = lblOK.getText();
//			
//			DefinicionUtilidades.softAssertionsDefinicion.assertThat("OK")
//			.as(String.format("El nombre de la reporte esperada %s no correponde al de pantalla que es %s",
//					"OK", validarName))
//			.isEqualTo(validarName);
			waitFor(5).seconds();
			getDriver().findElement(By.xpath(
					"/html/body/app-root/app-content/mat-sidenav-container/mat-sidenav-content/div/mat-card/app-reports/div/div[2]/mat-card/mat-card-content/div/div[2]/div[8]/span/button/span/mat-icon"))
					.click();
			waitFor(3).seconds();
			getDriver().findElement(By.xpath("//*[@id='cdk-overlay-3']/div/div/div/div[2]/div[2]")).click();

			btnConfirmaDelete.click();

		} catch (Exception e) {
			Mdl_variables.LOGGERTRANSVERSAL.log(Level.SEVERE, e.getMessage());
			DefinicionUtilidades.softAssertionsDefinicion.assertAll();

		}

	}

}
