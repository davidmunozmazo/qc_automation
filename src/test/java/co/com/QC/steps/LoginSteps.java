package co.com.QC.steps;

import co.com.QC.data.DataTest;
import co.com.QC.page.LoginPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.StepEventBus;
import net.thucydides.core.steps.ScenarioSteps;

@SuppressWarnings("serial")
public class LoginSteps extends ScenarioSteps {
	
	LoginPage loginpage;


	@Step
	public void abrirNavegador() {
			DataTest.obtenerDatos();
			loginpage.open();
			loginpage.ingresarUrl(DataTest.getStrUrl());
			StepEventBus.getEventBus().updateCurrentStepTitle("Se abre el browser para realizar la prueba");
	
}

	@Step
	public void ingresarCredenciales(String email, String password) {
		
			loginpage.ingresarCredenciales(email, password);
		
	}
	@Step
	public void verificoDashboard(String label) {
		try {
			loginpage.verificarLabel(label);
			
		} catch (Exception e) {
			
		}
		
	}
}
