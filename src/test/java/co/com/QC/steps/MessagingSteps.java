package co.com.QC.steps;

import co.com.QC.page.MessagingPage;
import co.com.QC.util.MetodosComunes;
import net.thucydides.core.annotations.Step;

public class MessagingSteps {

	MessagingPage messagingPage;
	
	@Step
	public void ingresarModulo() {
	messagingPage.ingresarModulo();
		
	}
	@Step
	public void buscarCarrier(String carrier) {
		messagingPage.buscarCarrier(carrier);
		
	}

	public void seleccionarCarrier() {
		messagingPage.seleccionar();
		
	}
	public void seleccionarContactos() {
		int contacto = MetodosComunes.seleccionarContacto();
		messagingPage.seleccionoContacto(contacto);
		
	}
	public void enviarMensaje() {
		messagingPage.enviarMensaje();
		
	}

}
