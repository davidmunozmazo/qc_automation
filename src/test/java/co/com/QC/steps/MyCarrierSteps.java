package co.com.QC.steps;

import co.com.QC.page.MyCarrierPage;
import net.thucydides.core.annotations.Step;

public class MyCarrierSteps {
	

	MyCarrierPage myCarrierPage;

	@Step
	public void darClickEnBoton() {
		myCarrierPage.darclickBoton();
		
		
	}
	@Step
	public void addCarrier() {
		myCarrierPage.addCarrier();
		
	}
	
	@Step
	public void buscarCarrier(String carrier) {
		myCarrierPage.buscarCarrier(carrier);
		
	}
	@Step
	public void adicionarALaLista() {
		myCarrierPage.adicionarLista();
		
	}
	@Step
	public void realizarBusqueda() {
		myCarrierPage.realizarBusqueda();
		
	}
	@Step
	public void encuentraEnLista(String carrier) {
		myCarrierPage.entrarEnLaLista(carrier);
		
	}

}
