package co.com.QC.steps;

import co.com.QC.data.DataTest;
import co.com.QC.page.MyGroupsPage;
import net.thucydides.core.annotations.Step;

public class MyGroupsSteps {
	
	
	MyGroupsPage myGroupsPage; 
	
	@Step
	public void ingresarAlmoduloGroups() {
		myGroupsPage.ingresarModuloGroups();
		
		
	}
	@Step
	public void ingresarGroups() {
		myGroupsPage.adicionarGroups();
		
	}
	@Step
	public void ingresarInformacion() {
		//DataTest.obtenerDatos();
		myGroupsPage.ingresarInformacion(DataTest.getStrReportName());
		
		
	}
	
	@Step
	public void buscarCarrier() {
		myGroupsPage.buscarCarrier(DataTest.getStrCarrier());
		
		
	}
	@Step
	public void validarcreacion() {
		myGroupsPage.validarCreacion(DataTest.getStrReportName());
		
	}
	@Step
	public void EliminarGrupo() {
		myGroupsPage.eliminarGrupo();
		
	}

}
