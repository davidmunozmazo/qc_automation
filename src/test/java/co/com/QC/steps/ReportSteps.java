package co.com.QC.steps;

import co.com.QC.data.DataTest;
import co.com.QC.page.ReportPage;
import net.thucydides.core.annotations.Step;

public class ReportSteps {

	static ReportPage reportPage;
	
	
	@Step
	public void clickEnBotton() {
			
	reportPage.clickEnBotonReport();
	
	}

	@Step
	public void adicionarReporte() {
		
		
		reportPage.agregarReporte();
		
		
	}
	@Step
	public void ingresarInformacion() {
	//	DataTest.obtenerDatos();
		reportPage.adicionarReporte(DataTest.getStrCorreo(), DataTest.getStrFrequency(), DataTest.getStrReportName(), DataTest.inputActive);
		
	}

	@Step
	public void ejecutarReporte() {
		reportPage.ejecutarReporte();
		
	}
	
	@Step
	public void validarCreacion() {
		reportPage.validarCreacion();
		
	}
	
	
	

}
