$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/features/messaging.feature");
formatter.feature({
  "name": "Ingresar al modulo My messaging",
  "description": "As\n\tUsuario\nI want\n\t crear un mensaje y enviarlo",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "estoy logueado en la aplicacion",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@funcional"
    }
  ]
});
formatter.step({
  "name": "doy click en el boton de messaging",
  "keyword": "Given "
});
formatter.match({
  "location": "MessagingDefinitions.doyClickEnElBotonDeMessaging()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "busco \"0014 Carrier\" carrier",
  "keyword": "When "
});
formatter.match({
  "location": "MessagingDefinitions.buscoCarrier(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "seleccion el carrier",
  "keyword": "And "
});
formatter.match({
  "location": "MessagingDefinitions.seleccionElCarrier()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "seleccion uno de los contactos",
  "keyword": "And "
});
formatter.match({
  "location": "MessagingDefinitions.seleccionUnoDeLosContactos()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "redacto el mensaje y envio el mensaje",
  "keyword": "Then "
});
formatter.match({
  "location": "MessagingDefinitions.redactoElMensajeYEnvioElMensaje()"
});
formatter.result({
  "status": "passed"
});
});